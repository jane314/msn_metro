# msn_metro

## Information
* [Information for Developers | Metro Transit, City of Madison, Wisconsin](https://www.cityofmadison.com/metro/business/information-for-developers).
* Static Data: `http://transitdata.cityofmadison.com/GTFS/mmt_gtfs.zip`
* Dynamic Data: `http://transitdata.cityofmadison.com/TripUpdate/TripUpdates.json`

## To build/run

Backend:
* Install Deno: https://deno.land/.
* Go to `./backend` and run `deno run --allow-net --allow-read Main.ts`.

Frontend:
* Install Node.js: https://nodejs.org/en/.
* Go to `./frontend` and run `npm i`, then `vite`.

