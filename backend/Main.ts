import { IIndexedObj, readCsvFileToIndexedObj } from "./JaneStdLib.ts";
import { serve, Server } from "https://deno.land/std@0.115.1/http/server.ts";

/*
 * TYPES
 */
type IObject = {
  [key: string]: any;
};

/*
 * LOADING DATA
 */
const trip_static_data: IObject = await readCsvFileToIndexedObj(
  "./example_data/trips.txt",
  "trip_id",
);

const stop_static_data: IObject = await readCsvFileToIndexedObj(
  "./example_data/stops.txt",
  "stop_id",
);

const stop_static_data2: string = Object.values(stop_static_data)
	.map( (entry: IObject) =>  entry['stop_code'] + '\t' + entry['stop_name'] ).join('\n');

let trip_dynamic_data: IObject = {};

async function loadTripDynamicData() {
  const res = await fetch(
    "http://transitdata.cityofmadison.com/TripUpdate/TripUpdates.json",
  );
  if (res.ok) {
    const data = await res.json();
    trip_dynamic_data = data;
  }
}

loadTripDynamicData();
setTimeout(loadTripDynamicData, 120 * 1000);

/*
 * MAIN EXECUTION
 */
async function readStop(stop_id: string) {
  const stop_data = trip_dynamic_data.entity.map((entry: IObject) => {
    const trip_id = entry["trip_update"]["trip"]["trip_id"];
    return entry["trip_update"]["stop_time_update"]
      .filter(
        (stop_arrival: IObject) => stop_arrival["stop_id"] === stop_id,
      )
      .map((entry: IObject) => {
        return {
          trip: trip_static_data[trip_id]["route_short_name"] + " " +
            trip_static_data[trip_id]["trip_headsign"],
          departure: new Date(entry.departure.time * 1000).toISOString(),
          "delay_str": entry.departure.delay === 0
            ? ""
            : (`${entry.departure.delay / 60} min ${
              entry.departure.delay > 0 ? "late" : "early"
            }`),
        };
      });
  })
    .flat();
  return {
    "stop_name": stop_static_data[stop_id]["stop_name"],
    stop_data,
  };
}

const handler = async function (request: Request): Promise<Response> {
  try {
    const path = new URL(request.url).pathname.split("/").slice(1);
    const headers = new Headers({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    });
    // HANDLE ERR TODO
    if (path[0] === "stop") {
      if (path.length === 1) {
        return new Response("404, sorry sir or madam", { status: 404 });
      }
      const stopdata = await readStop(path[1]);
      return new Response(JSON.stringify(stopdata), { status: 200, headers });
    } else if (path[0] === "allstops") {
      return new Response(JSON.stringify(stop_static_data2), {
        status: 200,
        headers,
      });
    } else {
      return new Response("404, sorry sir or madam", { status: 404 });
    }
  } catch (e) {
    console.error(e.message);
    return new Response("404, sorry sir or madam", { status: 404 });
  }
};

const server = new Server({ handler });
const listener = Deno.listen({
  port: 8000,
  // certFile: "./cert.pem",
  // keyFile: "./key.pem",
  // alpnProtocols: ["h2", "http/1.1"],
});
server.serve(listener);
